Various exceptions with a suggested HTTP Status Code and HTTP Status Message.

All exceptions are based on PHP internal exceptions, and HTTP Status Codes. Use
https://en.wikipedia.org/wiki/List_of_HTTP_status_codes for reference.

    try {
        throw new Charm\PageNotFoundError();
    } catch (Charm\ExceptionInterface $e) {
        header("HTTP/1.0 ".$e->getHttpCode()." ".$e->getHttpStatus());
        die();
    }
