<?php
declare(strict_types=1);

namespace Charm;

class NotExtendedError extends ClientError {
    protected $httpCode = 510;
    protected $httpStatus = "Not Extended";
}
