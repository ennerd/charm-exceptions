<?php
declare(strict_types=1);

namespace Charm;

class Exception extends \Exception implements ExceptionInterface {
    use ExceptionTrait;
    protected $httpCode = 500;
    protected $httpStatus = "Internal server error";
}
