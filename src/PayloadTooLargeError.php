<?php
declare(strict_types=1);

namespace Charm;

class PayloadTooLargeError extends ClientError {
    protected $httpCode = 413;
    protected $httpStatus = "Payload Too Large";
}
