<?php
declare(strict_types=1);

namespace Charm;

class FailedDependencyError extends ClientError {
    protected $httpCode = 424;
    protected $httpStatus = "Failed Dependency";
}
