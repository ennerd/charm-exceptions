<?php
declare(strict_types=1);

namespace Charm;

class HTTPVersionNotSupportedError extends ClientError {
    protected $httpCode = 505;
    protected $httpStatus = "HTTP Version Not Supported";
}
