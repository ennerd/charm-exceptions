<?php
declare(strict_types=1);

namespace Charm;

class MethodNotAllowedError extends ClientError {
    protected $httpCode = 405;
    protected $httpStatus = "Method Not Allowed";
}
