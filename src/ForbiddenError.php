<?php
declare(strict_types=1);

namespace Charm;

class ForbiddenError extends ClientError {
    protected $httpCode = 403;
    protected $httpStatus = "Forbidden";
}
