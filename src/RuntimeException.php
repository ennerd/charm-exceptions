<?php
declare(strict_types=1);

namespace Charm;

class RuntimeException extends \RuntimeException implements ExceptionInterface {
    use ExceptionTrait;

    protected $httpCode = 500;
    protected $httpStatus = "Internal runtime error";
}
