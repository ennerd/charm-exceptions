<?php
declare(strict_types=1);

namespace Charm;

class NotAcceptableError extends ClientError {
    protected $httpCode = 406;
    protected $httpStatus = "Not Acceptable";
}
