<?php
declare(strict_types=1);

namespace Charm;

class NetworkAuthenticationError extends ClientError {
    protected $httpCode = 511;
    protected $httpStatus = "Network Authentication Required";
}
