<?php
declare(strict_types=1);

namespace Charm;

class ServiceUnavailableError extends ClientError {
    protected $httpCode = 503;
    protected $httpStatus = "Service Unavailable";
}
