<?php
declare(strict_types=1);

namespace Charm;

class BadMethodCallException extends BadFunctionCallException {
    protected $httpCode = 500;
    protected $httpStatus = "Bad method call error";
}


