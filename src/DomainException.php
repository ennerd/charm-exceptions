<?php
declare(strict_types=1);

namespace Charm;

class DomainException extends LogicException {
    protected $httpCode = 500;
    protected $httpStatus = "Internal domain error";
}
