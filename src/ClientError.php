<?php
declare(strict_types=1);

namespace Charm;

class ClientError extends Exception {
    protected $httpCode = 400;
    protected $httpStatus = "Generic request error";
}
