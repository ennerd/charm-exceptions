<?php
declare(strict_types=1);

namespace Charm;

class BandwidthExceededError extends ServerError {
    protected $httpCode = 509;
    protected $httpStatus = "Bandwidth Exceeded";
}
