<?php
declare(strict_types=1);

namespace Charm;

class LoopDetectedError extends ClientError {
    protected $httpCode = 508;
    protected $httpStatus = "Loop Detected";
}
