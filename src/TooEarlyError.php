<?php
declare(strict_types=1);

namespace Charm;

class TooEarlyError extends ClientError {
    protected $httpCode = 425;
    protected $httpStatus = "Too Early";
}
