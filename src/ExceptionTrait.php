<?php
declare(strict_types=1);

namespace Charm;

trait ExceptionTrait {
    public function getHttpCode(): int {
        return $this->httpCode;
    }

    public function getHttpStatus(): string {
        return $this->httpStatus;
    }

    public function jsonSerialize() {
        $result = [
            'error' => $this->getMessage(),
            'code' => $this->getCode(),
            'http_code' => $this->getHttpCode(),
            'http_status' => $this->getHttpStatus(),
        ];
        if (defined('DEBUG') && DEBUG) {
            $result['file'] = $this->getFile();
            $result['line'] = $this->getLine();
            $result['trace'] = $this->getTrace();
            $result['previous'] = $this->getPrevious();
        }
        return $result;
    }
}
