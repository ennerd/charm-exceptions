<?php
declare(strict_types=1);

namespace Charm;

class BadRequestError extends ClientError {
    protected $httpCode = 400;
    protected $httpStatus = "Bad request";
}
