<?php
declare(strict_types=1);

namespace Charm;

class ProxyAuthenticationRequiredError extends ClientError {
    protected $httpCode = 407;
    protected $httpStatus = "Proxy Authentication Required";
}
