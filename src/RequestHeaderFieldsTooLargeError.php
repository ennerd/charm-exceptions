<?php
declare(strict_types=1);

namespace Charm;

class RequestHeaderFieldsTooLargeError extends ClientError {
    protected $httpCode = 431;
    protected $httpStatus = "Request Header Fields Too Large";
}
