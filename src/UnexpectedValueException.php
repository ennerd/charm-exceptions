<?php
declare(strict_types=1);

namespace Charm;

class UnexpectedValueException extends RuntimeException {
    protected $httpCode = 400;
    protected $httpStatus = "Unexpected value error";
}
