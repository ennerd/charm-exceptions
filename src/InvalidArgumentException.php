<?php
declare(strict_types=1);

namespace Charm;

class InvalidArgumentException extends LogicException {
    protected $httpStatus = "Invalid argument error";
}
