<?php
declare(strict_types=1);

namespace Charm;

class URITooLongErrorError extends ClientError {
    protected $httpCode = 414;
    protected $httpStatus = "URI Too Long";
}
