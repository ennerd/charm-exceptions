<?php
declare(strict_types=1);

namespace Charm;

class SiteIsOverloadedError extends ServerError {
    protected $httpCode = 529;
    protected $httpStatus = "Site Is Overloaded";
}

