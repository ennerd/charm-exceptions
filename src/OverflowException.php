<?php
declare(strict_types=1);

namespace Charm;

class OverflowException extends RuntimeException {
    protected $httpCode = 400;
    protected $httpStatus = "Value overflow error";
}
