<?php
declare(strict_types=1);

namespace Charm;

class NetworkReadTimeoutError extends ServerError {
    protected $httpCode = 598;
    protected $httpStatus = "Network Read Timeout";
}

