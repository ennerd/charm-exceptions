<?php
declare(strict_types=1);

namespace Charm;

class GatewayTimeoutError extends ClientError {
    protected $httpCode = 504;
    protected $httpStatus = "Gateway Timeout";
}
