<?php
declare(strict_types=1);

namespace Charm;

class LockedError extends ClientError {
    protected $httpCode = 423;
    protected $httpStatus = "Locked";
}
