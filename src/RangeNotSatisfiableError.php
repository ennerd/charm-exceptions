<?php
declare(strict_types=1);

namespace Charm;

class RangeNotSatisfiableError extends ClientError {
    protected $httpCode = 416;
    protected $httpStatus = "Range Not Satisfiable";
}
