<?php
declare(strict_types=1);

namespace Charm;

class GoneError extends ClientError {
    protected $httpCode = 410;
    protected $httpStatus = "Gone";
}
