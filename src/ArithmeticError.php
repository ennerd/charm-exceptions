<?php
declare(strict_types=1);

namespace Charm;

class ArithmeticError extends RuntimeException {
    protected $httpCode = 400;
    protected $httpStatus = "Arithmetic error";
}
