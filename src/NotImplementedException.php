<?php
declare(strict_types=1);

namespace Charm;

class NotImplementedException extends LogicException {
    protected $httpCode = 501;
    protected $httpStatus = "Functionality not implemented";
}
