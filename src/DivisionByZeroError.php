<?php
declare(strict_types=1);

namespace Charm;

class DivisionByZeroException extends RuntimeException {
    protected $httpCode = 400;
    protected $httpStatus = "Division by zero error";
}
