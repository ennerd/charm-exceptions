<?php
declare(strict_types=1);

namespace Charm;

class UnderflowException extends RuntimeException {
    protected $httpCode = 400;
    protected $httpStatus = "Value underflow error";
}
