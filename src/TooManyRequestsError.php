<?php
declare(strict_types=1);

namespace Charm;

class TooManyRequestsError extends ClientError {
    protected $httpCode = 429;
    protected $httpStatus = "Too Many Requests";
}
