<?php
declare(strict_types=1);

namespace Charm;

class RequestTimeoutError extends ClientError {
    protected $httpCode = 408;
    protected $httpStatus = "Request Timeout";
}
