<?php
declare(strict_types=1);

namespace Charm;

class PreconditionRequiredError extends ClientError {
    protected $httpCode = 428;
    protected $httpStatus = "Precondition Required";
}
