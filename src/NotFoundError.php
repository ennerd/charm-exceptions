<?php
declare(strict_types=1);

namespace Charm;

class NotFoundError extends ClientError {
    protected $httpCode = 404;
    protected $httpStatus = "Not Found";
}
