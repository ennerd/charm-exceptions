<?php
declare(strict_types=1);

namespace Charm;

use JsonSerializable;

interface ExceptionInterface extends JsonSerializable {
    public function getHttpCode(): int;
    public function getHttpStatus(): string;
}
