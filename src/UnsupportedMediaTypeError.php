<?php
declare(strict_types=1);

namespace Charm;

class UnsupportedMediaTypeError extends ClientError {
    protected $httpCode = 415;
    protected $httpStatus = "Unsupported Media Type";
}
