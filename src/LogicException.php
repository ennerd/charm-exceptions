<?php
declare(strict_types=1);

namespace Charm;

class LogicException extends \LogicException implements ExceptionInterface {
    use ExceptionTrait;

    protected $httpCode = 500;
    protected $httpStatus = "Internal logic error";
}
