<?php
declare(strict_types=1);

namespace Charm;

class BadGatewayError extends ClientError {
    protected $httpCode = 502;
    protected $httpStatus = "Bad Gateway";
}
