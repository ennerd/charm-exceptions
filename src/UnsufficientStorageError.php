<?php
declare(strict_types=1);

namespace Charm;

class InsufficientStorageError extends ClientError {
    protected $httpCode = 507;
    protected $httpStatus = "Insufficient Storage";
}
