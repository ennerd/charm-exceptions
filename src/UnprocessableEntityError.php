<?php
declare(strict_types=1);

namespace Charm;

class UnprocessableEntityError extends ClientError {
    protected $httpCode = 422;
    protected $httpStatus = "Unprocessable Entity";
}
