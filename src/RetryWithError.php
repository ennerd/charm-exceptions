<?php
declare(strict_types=1);

namespace Charm;

class RetryWithError extends ClientError {
    protected $httpCode = 449;
    protected $httpStatus = "Retry With";
}

