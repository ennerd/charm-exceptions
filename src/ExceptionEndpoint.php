<?php
declare(strict_types=1);

namespace F2\App;

use F2\App\Contracts\ResponseInterface;
use F2\App\{Response};

class ExceptionEndpoint extends AbstractEndpoint {

    public function __construct(App $app, \Throwable $exception) {
        
    }

    public function GET(array $query=[]): ResponseInterface {

    }

}
