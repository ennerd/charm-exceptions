<?php
declare(strict_types=1);

namespace Charm;

class PaymentRequiredError extends ClientError {
    protected $httpCode = 402;
    protected $httpStatus = "Payment Required";
}

