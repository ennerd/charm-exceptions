<?php
declare(strict_types=1);

namespace Charm;

class OutOfBoundsException extends RuntimeException {
    protected $httpCode = 400;
    protected $httpStatus = "Value out of bounds error";
}
