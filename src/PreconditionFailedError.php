<?php
declare(strict_types=1);

namespace Charm;

class PreconditionFailedError extends ClientError {
    protected $httpCode = 412;
    protected $httpStatus = "Precondition Failed";
}
