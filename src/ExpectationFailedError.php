<?php
declare(strict_types=1);

namespace Charm;

class ExpectationFailedError extends ClientError {
    protected $httpCode = 417;
    protected $httpStatus = "Expectation Failed";
}
