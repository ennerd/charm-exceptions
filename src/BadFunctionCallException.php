<?php
declare(strict_types=1);

namespace Charm;

class BadFunctionCallException extends LogicException {
    protected $httpCode = 500;
    protected $httpStatus = "Bad function call error";
}
