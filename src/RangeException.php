<?php
declare(strict_types=1);

namespace Charm;

class RangeException extends RuntimeException {
    protected $httpCode = 400;
    protected $httpStatus = "Value out of range error";
}
