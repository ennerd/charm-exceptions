<?php
declare(strict_types=1);

namespace Charm;

class ServerError extends Exception {
    protected $httpCode = 500;
    protected $httpStatus = "Internal Server Error";
}
