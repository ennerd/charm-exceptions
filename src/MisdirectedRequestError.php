<?php
declare(strict_types=1);

namespace Charm;

class MisdirectedRequestError extends ClientError {
    protected $httpCode = 421;
    protected $httpStatus = "Misdirected Request";
}
