<?php
declare(strict_types=1);

namespace Charm;

class PageExpiredError extends ClientError {
    protected $httpCode = 419;
    protected $httpStatus = "Page Expired";
}
