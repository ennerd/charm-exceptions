<?php
declare(strict_types=1);

namespace Charm;

class LengthRequiredError extends ClientError {
    protected $httpCode = 411;
    protected $httpStatus = "Length Required";
}
