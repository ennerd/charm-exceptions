<?php
declare(strict_types=1);

namespace Charm;

class LengthException extends LogicException {
    protected $httpStatus = "Invalid length specified";
}
