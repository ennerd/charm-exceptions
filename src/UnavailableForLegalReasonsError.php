<?php
declare(strict_types=1);

namespace Charm;

class UnavailableForLegalReasonsError extends ClientError {
    protected $httpCode = 451;
    protected $httpStatus = "Unavailable For Legal Reasons";
}
