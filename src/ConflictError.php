<?php
declare(strict_types=1);

namespace Charm;

class ConfictError extends ClientError {
    protected $httpCode = 409;
    protected $httpStatus = "Confict";
}
