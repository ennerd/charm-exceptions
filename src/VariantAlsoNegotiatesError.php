<?php
declare(strict_types=1);

namespace Charm;

class VariantAlsoNegotiatesError extends ClientError {
    protected $httpCode = 506;
    protected $httpStatus = "Variant Also Negotiates";
}
