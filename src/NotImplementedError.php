<?php
declare(strict_types=1);

namespace Charm;

class NotImplementedError extends ClientError {
    protected $httpCode = 501;
    protected $httpStatus = "Not Impemented";
}
