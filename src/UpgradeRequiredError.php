<?php
declare(strict_types=1);

namespace Charm;

class UpgradeRequiredError extends ClientError {
    protected $httpCode = 426;
    protected $httpStatus = "Upgrade Required";
}
